#!/bin/sh

apt update
apt install -y apache2
cp asu.conf /etc/apache2/sites-enabled/asu.conf
cp -r asu* /var/www/

chmod -R 777 /var/www/asu*

htpasswd -cb /var/www/htpasswd ada ada
htpasswd -b /var/www/htpasswd igor igor

systemctl restart apache2
