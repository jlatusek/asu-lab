#!/bin/sh

# apt update
# apt install -y nginx
cp nginx.conf /etc/nginx/sites-enabled/nginx.conf
cp -r asu* /var/www/

chmod -R 777 /var/www/asu*

printf "ada:$(openssl passwd -crypt ada)\n" > /var/www/htpasswd
printf "igor:$(openssl passwd -crypt igor)\n" >> /var/www/htpasswd

systemctl restart nginx
