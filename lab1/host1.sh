#!/bin/bash


## reczna konfiguracja sieci
sudo ifconfig eth0 192.168.1.10 netmask 255.255.255.0
sudo route add -net 192.168.2.0/24 gw 192.168.1.1 eth0
sudo dhclient eth1 -v
## 

cat << EOF >> /etc/network/interfaces
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet static
      address 192.168.1.10
      netmask 255.255.255.0
      up route add -net 192.168.2.0/24 gw 192.168.1.1 eth0

allow-hotplug eth1
iface eth1 inet dhcp
dns-nameserver 8.8.8.8
dns-nameserver 8.8.4.4
dns-nameserver 208.67.220.220
EOF

# restart interfejsów
ifdown eth0
ifdown eth1
ifup eth0
ifup eth1

## ftp
sudo apt install vsftpd
sudo systemctl enable vsftpd
sudo systemctl start vsftpd

sudo cp vsftpd.conf /etc/vsftpd.conf
systemctl restart vsftpd