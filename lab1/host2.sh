#!/bin/bash

## reczna konfiguracja sieci
sudo ifconfig eth0 192.168.2.20 netmask 255.255.255.0
sudo route add -net 192.168.1.0/24 gw 192.168.2.1 eth0
sudo dhclient eth1 -v
## 


cat << EOF >> /etc/network/interfaces
auto eth0
iface eth0 inet static
      address 192.168.2.20
      netmask 255.255.255.0
      gateway 192.168.2.1
      up route add -net 192.168.1.0/24 gw 192.168.2.1 eth0

allow-hotplug eth1
iface eth1 inet dhcp
dns-nameserver 8.8.8.8
dns-nameserver 8.8.4.4
dns-nameserver 208.67.220.220

EOF
# restart interfejsów
ifdown eth0
ifdown eth1
ifup eth10
ifup eth1

