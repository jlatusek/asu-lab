#!/bin/bash 

## reczna konfiguracja sieci
sudo ifconfig eth0 192.168.1.1 netmask 255.255.255.0
sudo ifconfig eth1 192.168.2.1 netmask 255.255.255.0
sudo dhclient eth2 -v

cat /proc/sys/net/ipv4/ip_forward #wyswitla aktualny stan forward
sudo sysctl -w net.ipv4.ip_forward=1 # ustawia na 1

## 

cat << EOF > /etc/network/interfaces
auto eth0
iface eth0 inet static
      address 192.168.1.1
      netmask 255.255.255.0

auto eth1
iface eth1 inet static
      address 192.168.2.1
      netmask 255.255.255.0

allow-hotplug eth2
iface eth2 inet dhcp
dns-nameserver 8.8.8.8
dns-nameserver 8.8.4.4
dns-nameserver 208.67.220.220
EOF
ifdown eth1
ifdown eth1
sleep 1
ifup eth2
ifup eth2


# dodanie w systemie na stałe wpisu pozwalającego na forward i zatwierdzenie go
echo net.ipv4.ip_forward = 1 | sudo tee -a /etc/sysctl.conf
sudo sysctl -p