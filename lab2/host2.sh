#!/bin/bash

sudo apt-get install bind9

cp /etc/resolv.conf /etc/resolv.conf.back

# .10 primary
# .20 secondary
# Możliwe że trzeba wyjebać wszystko poza nameserver :))
cat > /etc/resolv.conf << EOF
nameserver 192.168.4.10
EOF

cat > /etc/bind/named.conf.local << 'EOF'
zone "asu.ia.pw.edu.pl" {
	type slave;
	file "/etc/bind/db.asu.ia.pw.edu.pl";
	masters { 192.168.4.10; };
};

zone "4.168.192.in-addr.arpa" {
	type slave;
	file "/etc/bind/db.192";
	masters { 192.168.4.10; };
};
EOF

service bind9 restart

### zadanie 3
sudo apt install nfs-common
mkdir -p /host1

cat >> /etc/fstab << 'EOF'
192.168.4.10:/pub /host1 nfs defaults 0 0 
EOF

mount --all
