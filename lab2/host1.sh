#!/bin/bash

sudo apt-get install bind9
sudo apt-get install dnsutils

cat > /etc/bind/named.conf.local << 'EOF'
zone "asu.ia.pw.edu.pl" {
	type master;
	file "/etc/bind/db.asu.ia.pw.edu.pl";
	allow-transfer { 192.168.4.20; };
};

zone "4.168.192.in-addr.arpa" {
	type master;
	file "/etc/bind/db.192";
	allow-transfer { 192.168.4.20; };
};
EOF
cp /etc/bind/db.local /etc/bind/db.asu.ia.pw.edu.pl

cat > /etc/bind/db.asu.ia.pw.edu.pl << 'EOF'
$TTL	604800
@	IN	SOA	asu.ia.pw.edu.pl. root.asu.ia.pw.edu.pl. (
			7		; Serial
			604800		; Refresh
			86400		; Retry
			2419200		; Expire
			604800 )	; Negative Cache TTL
;
@	IN	NS	ns.asu.ia.pw.edu.pl.
ns	IN	A	192.168.4.10

host1.asu.ia.pw.edu.pl.	IN	A	192.168.4.10
host2.asu.ia.pw.edu.pl.	IN	A	192.168.4.20
EOF

cp /etc/bind/db.127 /etc/bind/db.192

cat > /etc/bind/db.192 << 'EOF'
$TTL    604800
@     IN      SOA     ns.asu.ia.pw.edu.pl. root.asu.ia.pw.edu.pl. (
				5         ; Serial
				604800    ; Refresh
				86400     ; Retry
				2419200   ; Expire
				604800 )  ; Negative Cache TTL
;
@	IN      NS      ns.
10	IN      PTR     ns.asu.ia.pw.edu.pl.
10	IN      PTR     host1.asu.ia.pw.edu.pl.
20	IN      PTR     host2.asu.ia.pw.edu.pl.
EOF

service bind9 restart

### zadanie 3

sudo apt install nfs-kernel-server
mkdir -p /pub
echo "madzia" > /pub/madzia

cat > /etc/exports << 'EOF'
/pub *(rw,sync,no_root_squash)
EOF

chmod 777 /pub

service nfs-kernel-server start